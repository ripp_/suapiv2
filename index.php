<?php
/*
* SUCS SU-APIv2
* Imran Hussain - imranh@sucs.org
* Stuart John Watson - ripp_@sucs.org
*
* Use to lookup SUCS membership data from the Swansea University Students Union
* website. Calls a shell script which is just a curl command that logins in and
* saves the cookies to a file. Calls the next script which uses those cookies to
* curl the curl website again, quering it for memebrship data related to SUCS in
* XML format. It's then passed to xml2json where it is then echo'd out.
* The SUCS site then does its thang with it all
*/

// We want errors! When do we want them? Now!
error_reporting(E_ALL);
ini_set('display_errors', 1);

// path to scripts
$PATH = "./";

// get teh login cookie
exec($PATH."getcookie.sh");

// get the xml convert it to json and spit it out!
echo(shell_exec($PATH."dumpdata.sh"));

?>
