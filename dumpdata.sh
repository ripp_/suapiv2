#!/bin/bash

eub=$(curl -s 'https://www.swansea-union.co.uk/organisation/salesreports/6613/' -b ../cookies.txt --data '__EVENTTARGET=ctl00%24ctl00%24Main%24AdminPageContent%24lbPurchasers&__EVENTARGUMENT=&__VIEWSTATEGENERATOR=9B3E427D&ctl00%24ctl00%24Main%24AdminPageContent%24drDateRange%24txtFromDate=01%2F09%2F2015&ctl00%24ctl00%24Main%24AdminPageContent%24drDateRange%24txtFromTime=00%3A00&ctl00%24ctl00%24Main%24AdminPageContent%24drDateRange%24txtToDate=31%2F08%2F2016&ctl00%24ctl00%24Main%24AdminPageContent%24drDateRange%24txtToTime=00%3A00&ctl00%24ctl00%24Main%24AdminPageContent%24ReportViewer1%24ctl09%24VisibilityState%24ctl00=ReportPage' --compressed | grep -Po '(?<="ExportUrlBase":")[^"]+' | sed -e 's/\\u0026/\&/g')

#echo $eub

curl -s "https://www.swansea-union.co.uk${eub}XML" -b ../cookies.txt | xml2json
